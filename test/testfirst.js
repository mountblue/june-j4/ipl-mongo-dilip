let expect = require('chai').expect;
let firstQue = require('../scripts/firstQue.js');



describe("Matches Per Year", function () {

    xit("database connection found or not!", function () {
        expectedResult = "connection established!";
        let url = "mongodb://localhost:27017";
        let result = "";
        try {
            result = iplmatches.connectionToDatabase(url);
            // console.log(result)
        } catch (e) {
            console.log("Database connection failed!!");
        }
        expect(result).equal(expectedResult);
    })

    it("return exact team and season", async function () {
        let expectedOutput =  [
              {
                "label": 2008,
                "y": 6
              },
              {
                "label": 2010,
                "y": 3
              },
              {
                "label": 2011,
                "y": 1
              }
            ]

        result = await firstQue.numOfMatchesPlyedPerYear("testingdb", "matchestest");
        expect(result).deep.equal(expectedOutput);
    })
})