let expect = require('chai').expect;
let thirdQue = require('../scripts/thirdQue.js');
let MongoClient = require('mongodb').MongoClient;

describe("Matches Won Of All Teams Over All The Years", function () {
    it("Should return Exact value", async function () {
        let expectedResult = [{
                "label": "Mumbai Indians",
                "y": 4
              },
              {
                "label": "Rising Pune Supergiant",
                "y": 8
              },
              {
                "label": "Sunrisers Hyderabad",
                "y": 6
              },
              {
                "label": "Royal Challengers Bangalore",
                "y": 7
              }
        ];
        result = await thirdQue.extraRunConcededPerTeam("testingdb", "matchestest", "deliveriestest", 2008);
        expect(result).deep.equal(expectedResult);
    })
})