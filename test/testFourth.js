let expect = require('chai').expect;
let fourthQue = require('../scripts/fourthQue.js');


describe("Extra run conceded per team ", function () {
    it("Should return Exact value", async function () {
        let expectedResult = [{
                "label": "Bipul Sharma",
                "y": 4
              },
              {
                "label": "R Bhatia",
                "y": 4.666666666666667
              },
              {
                "label": "YS Chahal",
                "y": 5.5
              },
              {
                "label": "JJ Bumrah",
                "y": 6
              },
              {
                "label": "DJ Hooda",
                "y": 7
              },
              {
                "label": "B Kumar",
                "y": 7
              },
              {
                "label": "Imran Tahir",
                "y": 7
              },
              {
                "label": "TS Mills",
                "y": 8
              },
              {
                "label": "TG Southee",
                "y": 8.5
              },
              {
                "label": "A Zampa",
                "y": 9
              }
            ]   
        result = await fourthQue.topEconomicBowlers("testingdb", "matchestest", "deliveriestest", 2008);
        expect(result).deep.equal(expectedResult);
    })
})