let expect = require('chai').expect;
let secondQue = require('../scripts/secondQue.js');

describe("Matches Won Of All Teams Over All The Years", function () {
    it("Should return Exact value", async function () {
        let expectedResult = [ {
                "_id": "Mumbai Indians",
                "team": [
                  {
                    "label": 2010,
                    "y": 1
                  },
                  {
                    "label": 2011,
                    "y": 1
                  }
                ]
              },
              {
                "_id": "Kolkata Knight Riders",
                "team": [
                  {
                    "label": 2008,
                    "y": 1
                  }
                ]
              },
              {
                "_id": "Royal Challengers Bangalore",
                "team": [
                  {
                    "label": 2008,
                    "y": 1
                  }
                ]
              },
              {
                "_id": "Rising Pune Supergiant",
                "team": [
                  {
                    "label": 2008,
                    "y": 1
                  }
                ]
              },
              {
                "_id": "Kings XI Punjab",
                "team": [
                  {
                    "label": 2008,
                    "y": 1
                  },
                  {
                    "label": 2010,
                    "y": 1
                  }
                ]
              },
              {
                "_id": "Delhi Daredevils",
                "team": [
                  {
                    "label": 2010,
                    "y": 1
                  }
                ]
              },
              {
                "_id": "Sunrisers Hyderabad",
                "team": [
                  {
                    "label": 2008,
                    "y": 2
                  }
                ]
              }
        ]

        result = await secondQue.wonMatchesPerSeason("testingdb", "matchestest");
        expect(result).deep.equal(expectedResult);
    })
})