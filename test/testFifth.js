let expect = require('chai').expect;
let fifthQue = require('../scripts/fifthQue.js');



describe("Batsman data of all season", function () {
    it("return exact team and season", async function () {
        let expectedResult = [{
            "four": 2,
            "run": 14,
            "six": 1,
            "strikeRate": 155.55555555555557,
            "year": {
              "season": 2008
            }
          }]

        result = await fifthQue.batsmanDetailsInIPL("testingdb", "matchestest", "deliveriestest", "DA Warner");
        expect(result).deep.equal(expectedResult);
    })
})