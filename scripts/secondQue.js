let connectionObj = require('./connection.js');

function wonMatchesPerSeason(database, mCollection) {
    return new Promise(async function (resolve, reject) {
        let DBWithConnection = await connectionObj.getConnection(database)
        let matchesCollection = DBWithConnection[0].collection(mCollection);

        let findMatchesPerSeason = {
            $group: {
                _id: {
                    "seasons": "$season",
                    "teams": "$winner"
                },
                "count": {
                    $sum: 1
                }
            }
        }
        let takingSpecificColumn = {
            $project: {
                season: '$_id.seasons',
                team: '$_id.teams',
                count: '$count',
                _id: 0
            }
        }
        let groupingTeamAndSeason = {
            $group: {
                _id: '$team',
                team: {
                    $push: {
                        y: "$count",
                        label: "$season"
                    }
                }
            }
        }
        
        matchesCollection.aggregate([findMatchesPerSeason,takingSpecificColumn,groupingTeamAndSeason]).toArray(function (err, matchPerYear) {
            if (err) {
                console.log(err.message);
            } else if (matchPerYear.length > 0) {
                DBWithConnection[1].close();
                resolve(matchPerYear);
            }
        });
    })
}

function chartFormating(matchesData) {
    let mainTeam = []
    for (let i = 0; i < matchesData.length; i++) {
        let team = []
        let seasonRecord = []
        team[0] = matchesData[i]["_id"];
        for (let year = 2008; year <= 2017; year++) {
            for (j = 0; j < matchesData[i]["team"].length; j++) {
                if (matchesData[i]["team"][j]["label"] != year) {
                    let obj = {
                        y: 0,
                        label: year
                    }
                    seasonRecord.push(obj);
                    break;
                } else {
                    let obj = {
                        y: matchesData[i]["team"][j]["y"],
                        label: matchesData[i]["team"][j]["label"]
                    }
                    seasonRecord.push(obj);
                    break;
                }
            }
        }
        team[1] = seasonRecord;
        mainTeam.push(team);
        team = [];
    }
    return mainTeam;
}

module.exports = {
    wonMatchesPerSeason: wonMatchesPerSeason,
    chartFormating: chartFormating
}