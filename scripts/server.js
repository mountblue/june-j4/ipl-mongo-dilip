const first = require('./firstQue')
const second = require('./secondQue')
const third = require('./thirdQue')
const fourth = require('./fourthQue')
const fifth = require('./fifthQue.js');

function fileExists(filePath) {
    try {
        return require('fs').statSync(filePath).isFile();
    } catch (err) {
        console.log(filePath+" File Not Found!!")
        return false;
    }
}

function createAllJsonFiles() {
    if (!fileExists('../jsonfiles/numOfMatchesPerSeason.json')) {
        first.numOfMatchesPlyedPerYear("IPLData", "matches").then(function (data) {
            createJsonFile(data, "numOfMatchesPerSeason");
        })
    }
    if (!fileExists('../jsonfiles/wonMatches.json')) {
        second.wonMatchesPerSeason("IPLData", "matches").then(function (data) {
            let formedData = second.chartFormating(data);
            createJsonFile(formedData, "wonMatches");
        });
    }
    if (!fileExists('../jsonfiles/extraRunConcededPer.json')) {
        third.extraRunConcededPerTeam("IPLData", "matches", "deliveries", 2016).then(function (data) {
            createJsonFile(data, "extraRunConcededPer");
        })
    }
    if (!fileExists('../jsonfiles/topEconomicBowlers.json')) {
        fourth.topEconomicBowlers("IPLData", "matches", "deliveries", 2015).then(function (data) {
            createJsonFile(data, "topEconomicBowlers");
        })
    }
    if (!fileExists('../jsonfiles/batsman/v-kohliTest.json')) {
        fifth.batsmanDetailsInIPL("IPLData", "matches", "deliveries", "V Kohli").then(function (data) {
            createJsonFile(data, "batsman/v-kohliTest");
        })
    }
}
createAllJsonFiles();
function createJsonFile(data, fileName) {
    require('fs').writeFile("../jsonfiles/" + fileName + ".json", JSON.stringify(data, null, 4), (err) => {
        if (err) {
            console.log(err);
            return;
        }
        console.log(fileName + " File Created");
    });
}

