let connectionObj = require('./connection.js');

function topEconomicBowlers(database, mCollection, dCollection, year) {
    return new Promise(async function (resolve, reject) {
        let DBWithConnection = await connectionObj.getConnection(database)
        let matchesCollection = DBWithConnection[0].collection(mCollection);

        let getSeason = {
            $match: {
                season: year
            }
        }
        let joinDeliveryFile = {
            $lookup: {
                from: dCollection,
                localField: "id",
                foreignField: "match_id",
                as: "match"
            }
        }
        let flatMatchArray = {
            $unwind: "$match"
        }
        let makeGroupThenCountBallAndRun = {
            $group: {
                _id: "$match.bowler",
                totalRuns: {
                    $sum: "$match.total_runs"
                },
                totalBalls: {
                    $sum: 1
                },
                totalNoBalls: {
                    $sum: {
                        $cond: {
                            if: {
                                $ne: ["$match.noball_runs", 0]
                            },
                            then: 1,
                            else: 0
                        }
                    }
                },
                totalWideBalls: {
                    $sum: {
                        $cond: {
                            if: {
                                $ne: ["$match.wide_runs", 0]
                            },
                            then: 1,
                            else: 0
                        }
                    }
                }
            }
        }
        let findEconomicRate = {
            $project: {
                _id: 0,
                y: {
                    $multiply: [{
                        $divide: ["$totalRuns", {
                            $subtract: [{
                                $subtract: ["$totalBalls", "$totalNoBalls"]
                            }, "$totalWideBalls"]
                        }]
                    }, 6]
                },
                label: "$_id"
            }
        }
        let sortByEconomic = {
            $sort: {
                "y": 1
            }
        }
        let setLimit = {
            $limit: 10
        }

        matchesCollection.aggregate([getSeason, joinDeliveryFile, flatMatchArray, makeGroupThenCountBallAndRun,
            findEconomicRate, sortByEconomic, setLimit
        ]).toArray(function (err, economicBowler) {
            if (err) {
                console.log(err.message);
            } else if (economicBowler.length > 0) {
                DBWithConnection[1].close();
                resolve(economicBowler);
            }
        });
    })
}

module.exports = {
    topEconomicBowlers: topEconomicBowlers
}

