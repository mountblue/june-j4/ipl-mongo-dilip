
let connectionObj = require('./connection.js');

function extraRunConcededPerTeam(database,mCollection,dCollection,year) {
    return new Promise(async function (resolve, reject) {
        let DBWithConnection = await connectionObj.getConnection(database)
        let matchesCollection = DBWithConnection[0].collection(mCollection);

        let matchSeason = {
            $match: {
                season: year
            }
        }
        let joinDeliveryCollection = {
            $lookup: {
                from: dCollection,
                localField: "id",
                foreignField: "match_id",
                as: "match"
            }
        }
        let flatArray = {
            $unwind: "$match"
        }
        let countBowlingTeamExtraRun = {
            $group: {
                _id: "$match.bowling_team",
                totalExtraRuns: {
                    $sum: "$match.extra_runs"
                }
            }
        }
        let renameField = {
            $project: {
                _id: 0,
                y: "$totalExtraRuns",
                label: "$_id"
            }
        }

        matchesCollection.aggregate([matchSeason,joinDeliveryCollection,flatArray,countBowlingTeamExtraRun,renameField]).toArray(function (err, extraRun) {
        if (err) {
            console.log(err.message);
        } else if (extraRun.length > 0) {
            DBWithConnection[1].close();
            resolve(extraRun);
        }
    });
})
}

module.exports = {
    extraRunConcededPerTeam:extraRunConcededPerTeam
}