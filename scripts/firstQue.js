let connectionObj = require('./connection.js');

function numOfMatchesPlyedPerYear(database, mCollection) {
    return new Promise(async function (resolve, reject) {
        let DBWithConnection = await connectionObj.getConnection(database)
        let matchesCollection = DBWithConnection[0].collection(mCollection);
        let findNumberOfMatch = {
            "$group": {
                _id: "$season",
                totalMatches: {
                    $sum: 1
                }
            }
        }
        let sortAscendingOrder = {
            $sort: {
                "_id": 1
            }
        }
        let renameId = {
            $project: {
                _id: 0,
                label: "$_id",
                y:"$totalMatches"
            }
        }
        matchesCollection.aggregate([findNumberOfMatch, sortAscendingOrder, renameId]).toArray(function (err, matchPerYear) {
            if (err) {
                console.log(err.message);
            } else if (matchPerYear.length > 0) {
                DBWithConnection[1].close();
                resolve(matchPerYear);
            }
        });
    })
}

module.exports = {
    numOfMatchesPlyedPerYear: numOfMatchesPlyedPerYear
}