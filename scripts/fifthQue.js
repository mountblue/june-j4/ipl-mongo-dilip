let connectionObj = require("./connection.js");

function batsmanDetailsInIPL(database, mCollection, dCollection, batsmanName) {
    return new Promise(async function (resolve, reject) {
        let DBWithConnection = await connectionObj.getConnection(database)
        let matchesCollection = DBWithConnection[0].collection(mCollection);

        let joinDeliveriesCollection = {
            $lookup: {
                from: dCollection,
                localField: "id",
                foreignField: "match_id",
                as: "matches"
            }
        }
        let flatMatchesArray = {
            $unwind: "$matches"
        }
        let matchBatsman = {
            $match: {
                "matches.batsman": batsmanName
            }
        }
        let makeGroupAndCountRunAndDetails = {
            $group: {
                "_id": {
                    "season": "$season"
                },
                "totalRun": {
                    $sum: "$matches.batsman_runs"
                },
                "totalBall": {
                    $sum: 1
                },
                "four": {
                    $sum: {
                        $cond: {
                            if: {
                                $eq: ["$matches.batsman_runs", 4]
                            },
                            then: 1,
                            else: 0
                        }
                    }
                },
                "six": {
                    $sum: {
                        $cond: {
                            if: {
                                $eq: ["$matches.batsman_runs", 6]
                            },
                            then: 1,
                            else: 0
                        }
                    }
                }
            }
        }
        let strikeRate = {
            $project: {
                _id: 0,
                year: "$_id",
                run: "$totalRun",
                four: "$four",
                six: "$six",
                strikeRate: {$multiply: [{ $divide: ["$totalRun", "$totalBall"]}, 100]}
            }
        }
        matchesCollection.aggregate([joinDeliveriesCollection, flatMatchesArray, matchBatsman,
             makeGroupAndCountRunAndDetails,strikeRate]).toArray(function (err, economicBowler) {
            if (err) {
                console.log("Data not Found!")
                console.log(err.message);
                reject(err);
            } else if (economicBowler.length > 0) {
                DBWithConnection[1].close();
                resolve(economicBowler);
            }
        });
    })
}


module.exports = {
    batsmanDetailsInIPL: batsmanDetailsInIPL
}