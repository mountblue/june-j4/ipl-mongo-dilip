const MongoClient = require('mongodb').MongoClient;

function getConnection(dataBase) {
    return new Promise(function (resolve, reject) {
        const url = 'mongodb://localhost:27017';
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, connection) {
            if (err) {
                console.log("Connection not available!");
                reject(err);
            } else {
                const database = connection.db(dataBase);
                resolve([database, connection]);
            }
        })
    })
}

module.exports = {
    getConnection: getConnection
}